﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ListMovies
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailPage : ContentPage
    {
        public moviesModel movie { get; set; }
        public DetailPage(moviesModel m)
        {
            InitializeComponent();
            movie = m;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            this.imgImagen.Source = movie.image;
            this.lblNombre.Text = movie.title;
            this.lblDescripcion.Text = "Descripción: " + movie.description;
            this.lblCategoria.Text = "Categoría: " + movie.category;
            this.lblRating.Text = "Rating: 4";
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }
    }
}