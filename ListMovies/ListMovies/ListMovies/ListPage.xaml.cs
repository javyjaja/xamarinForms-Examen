﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ListMovies
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListPage : ContentPage
	{
        public string tocken { get; set; }

        public ListPage (string tocken)
		{
			InitializeComponent ();
            this.tocken = tocken;
            //var p = Navigation.NavigationStack.Count;
            //var a = Navigation.NavigationStack[p - 1].GetType().ToString();
            //var o = Navigation.NavigationStack[p - 2].GetType().ToString();
            
            
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            Conexion.tocken = this.tocken;
            Conexion.getMovies((movies) =>
            {

                var adapter = new ObservableCollection<moviesModel>();
                foreach (var item in movies)
                {
                    adapter.Add(item);
                }

                this.ltvMovies.ItemsSource = adapter;

                this.ltvMovies.RowHeight = 50;

            },
               () =>
               {
                   DisplayAlert("Error", "Ocurrió un error al consumir el servicio Web", "Ok");
               }
           );
        }

        private void subDetail(object sender,ItemTappedEventArgs e)
        {
            moviesModel m = (moviesModel)e.Item;
            Navigation.PushAsync(new DetailPage(m));
        }
    }
}