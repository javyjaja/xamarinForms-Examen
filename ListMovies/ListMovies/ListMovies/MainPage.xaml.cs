﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ListMovies
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        async void subLogin(object sender, EventArgs e)
        {
            if (txtName.Text == string.Empty || txtPassword.Text == string.Empty)
            {
                await DisplayAlert("Error", "No puede quedar vacio alguno de los campos", "Ok");
            }
            else
            {
                var u = new userModel();
                u.password = this.txtPassword.Text;
                u.username = this.txtName.Text;

                userModel usermodel = await Conexion.Login(u);
                
                if (usermodel.username != null)
                {
                    await Navigation.PushAsync(new ListPage(usermodel._kmd.authtoken));
                }
                else
                {
                    await DisplayAlert("Error", "Esta incorrecto tu usuario o contraseña", "Ok");
                }
            }
         }

        async void subRegister(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RegisterPage());
        }

    }
}
