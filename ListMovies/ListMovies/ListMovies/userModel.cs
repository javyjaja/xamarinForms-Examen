﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ListMovies
{
    public class userModel
    {
        public string nombre { get; set; }
        public string username { get; set; }
        public string telefono { get; set; }
        public string mail { get; set; }
        public _kmd _kmd { get; set; }
        public string password { get; set; }
        public string passwordConfirm { get; set; }
    }

    public class _kmd
    {
        public string authtoken { get; set; }
    }
}
