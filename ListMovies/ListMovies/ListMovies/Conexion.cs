﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ListMovies
{
    public class Conexion
    {
        public static string tocken { get; set; }

        public Conexion()
        {
        }

        public static async void getMovies(Action<moviesModel[]> success,
                                     Action error)
        {
            String url = "https://baas.kinvey.com/appdata/kid_SyXkBdMVM/movies";

            HttpWebRequest request =
                (HttpWebRequest)WebRequest.Create(new Uri(url));

            request.Headers["Authorization"] = "Kinvey " + tocken;
            request.Headers["X-Kinvey-API-Version"] = "3";

            request.Method = "GET";

            using (WebResponse response = await request.GetResponseAsync())
            {
                using (Stream stream = response.GetResponseStream())
                {

                    StreamReader reader = new StreamReader(stream);
                    String json = reader.ReadToEnd();

                    moviesModel[] movies = JsonConvert.DeserializeObject<moviesModel[]>(json);

                    success(movies);

                }
            }
        }

        public static async Task<userModel> Login(userModel u)
        {
            string url = "https://baas.kinvey.com/user/kid_SyXkBdMVM/login";

            HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(url);
            Encoding encoding = new UTF8Encoding();

            string postData = "{\"username\":\""+u.username+"\",\"password\":\""+u.password+"\"}";
            byte[] data = encoding.GetBytes(postData);

            httpWReq.ProtocolVersion = HttpVersion.Version11;
            httpWReq.Method = "POST";
            httpWReq.ContentType = "application/json";
            httpWReq.Headers[HttpRequestHeader.Authorization] = "Basic a2lkX1N5WGtCZE1WTToyOGQwOThmOTQ0N2Q0OWNmYmI0MTUwN2FjOWU3MDZiOA==";
            httpWReq.Headers["X-Kinvey-API-Version"] = "3";
            httpWReq.ContentLength = data.Length;

            Stream stream = httpWReq.GetRequestStream();
            stream.Write(data, 0, data.Length);
            stream.Close();
            try
            {
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                        
                String jsonResponse = reader.ReadToEnd();

                return ( JsonConvert.DeserializeObject<userModel>(jsonResponse));
            }
            catch (Exception)
            {
                return new userModel();
            }
        }

        public static async Task<userModel> subRegister(userModel u)
        {
            string url = "https://baas.kinvey.com/user/kid_SyXkBdMVM";
            HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(url);
            Encoding encoding = new UTF8Encoding();

            string postData = "{\"username\": \""+u.username+"\",\"password\":\""+u.password+"\",\"name\":\""+u.nombre+"\",\"phone\":\""+u.telefono+"\",\"mail\":\""+u.mail+"\"}";
            byte[] data = encoding.GetBytes(postData);

            httpWReq.ProtocolVersion = HttpVersion.Version11;
            httpWReq.Method = "POST";
            httpWReq.ContentType = "application/json";
            httpWReq.Headers[HttpRequestHeader.Authorization] = "Basic a2lkX1N5WGtCZE1WTToyOGQwOThmOTQ0N2Q0OWNmYmI0MTUwN2FjOWU3MDZiOA==";
            httpWReq.Headers["X-Kinvey-API-Version"] = "3";
            httpWReq.ContentLength = data.Length;

            Stream stream = httpWReq.GetRequestStream();
            stream.Write(data, 0, data.Length);
            stream.Close();
            try
            {
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());

                String jsonResponse = reader.ReadToEnd();

                return (JsonConvert.DeserializeObject<userModel>(jsonResponse));
            }
            catch (Exception)
            {
                return new userModel();
            }
        }

    }
}
