﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ListMovies
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();
        }

        async void subRegister(object sender, EventArgs e)
        {
            if (txtNombre.Text == string.Empty ||
                txtUsuario.Text == string.Empty ||
                txtTelefono.Text == string.Empty ||
                txtMail.Text == string.Empty ||
                txtPassword.Text == string.Empty ||
                txtPasswordConfi.Text == string.Empty
                )
            {
                await DisplayAlert("Error", "No puede quedar vacio alguno de los campos", "Ok");
            }
            else
            {
                if (txtPassword.Text != txtPasswordConfi.Text)
                {
                    await DisplayAlert("Error", "No coinciden las contraseñas", "Ok");
                }
                else
                {
                    userModel usuario = new userModel();
                    usuario.username = txtUsuario.Text;
                    usuario.nombre = txtNombre.Text;
                    usuario.telefono = txtTelefono.Text;
                    usuario.mail = txtMail.Text;
                    usuario.password = txtPassword.Text;

                    var registro = await Conexion.subRegister(usuario);
                    if (registro.username != null)
                    {
                        await Navigation.PushAsync(new ListPage(registro._kmd.authtoken));
                    }
                    else
                    {
                        await DisplayAlert("Error", "Ocurrio algun error en el servicio", "Ok");
                    }
                }
            }
        }
    }
}